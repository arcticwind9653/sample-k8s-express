FROM node:8-alpine

WORKDIR /usr/src/app

# ENV NODE_ENV=production
COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 8080
ENTRYPOINT [ "node", "src/app.js" ]

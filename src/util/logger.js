const winston = require('winston')
const httpContext = require('express-http-context')

winston.loggers.add('standard', {
  level: 'debug',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console({ format: winston.format.json() })
  ]
})
const stdLogger = winston.loggers.get('standard')

function writeLog (level, message, meta = {}) {
  stdLogger.log(level, message, Object.assign({}, meta, (httpContext.get('trace-headers') || {})))
}

const logger = {
  error: function (message, meta = {}) {
    writeLog('error', message, meta)
  },
  warn: function (message, meta = {}) {
    writeLog('warn', message, meta)
  },
  verbose: function (message, meta = {}) {
    writeLog('verbose', message, meta)
  },
  info: function (message, meta = {}) {
    writeLog('info', message, meta)
  },
  debug: function (message, meta = {}) {
    writeLog('debug', message, meta)
  },
  silly: function (message, meta = {}) {
    writeLog('silly', message, meta)
  }
}

module.exports = logger

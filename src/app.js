const express = require('express')
const httpContext = require('express-http-context')
const os = require('os')
const asyncHandler = require('express-async-handler')
const { createTerminus } = require('@godaddy/terminus')
const delay = require('delay')
const request = require('request-promise')
const logger = require('./util/logger')

const promBundle = require("express-prom-bundle");
const metricsMiddleware = promBundle({includeMethod: true, metricType: 'summary'});

const app = express()
app.enable('trust proxy')
app.use(metricsMiddleware);
app.use(httpContext.middleware)
let status = {
  shutdown: false
}

const FORWARD_HEADERS = [
  'x-request-id',
  'x-b3-traceid',
  'x-b3-spanid',
  'x-b3-parentspanid',
  'x-b3-sampled',
  'x-b3-flags',
  'x-ot-span-context'
]
const TRACE_HEADERS = [
  'x-request-id',
  'x-b3-traceid',
  'x-b3-spanid',
  'x-b3-parentspanid'
]
// capture header for trace
app.use(function (req, res, next) {
  let traceHeader = {}
  Object.keys(req.headers).map(function (key, index) {
    if (TRACE_HEADERS.indexOf(key) !== -1) {
      traceHeader[key] = req.headers[key]
    }
  })
  httpContext.set('trace-headers', traceHeader)
  // if (!req.headers['user-agent'] || !req.headers['user-agent'].includes('kube-probe')) {
  //   logger.debug('Request Header', req.headers)
  // }
  next()
})

app.get('/', asyncHandler(async (req, res, next) => {
  logger.info(status.shutdown ? `Farewell request for ${req.ip}` : `Received request from ${req.ip}`)
  res.json({success: true, msg: `You've hit ${os.hostname()} from ${req.ip}. Ver:${process.env.APP_VERSION}. var1=${process.env.SAMPLE_VAR_1}. secert=${process.env.SAMPLE_SECRET_1}`})
}))

app.get('/public/:proxy', asyncHandler(async (req, res, next) => {
  res.json({success: true, msg: `Public with input ${JSON.stringify(req.params)}`})
}))

app.get('/v1/app/public/:proxy', asyncHandler(async (req, res, next) => {
  res.json({success: true, msg: `Public with input ${JSON.stringify(req.params)}`})
}))

app.get('/error', asyncHandler(async (req, res, next) => {
  let error = new Error('Something Broken')
  error.status = 503
  throw error
}))

function getForwardHeader (orgHeader) {
  let header = {}
  Object.keys(orgHeader).map(function (key, index) {
    if (FORWARD_HEADERS.indexOf(key) !== -1) {
      header[key] = orgHeader[key]
    }
  })
  return header
}

async function callService (endpoint, reqHeader) {
  try {
    logger.info(`Calling service ${endpoint}`)
    // logger.profile(`Profile service ${endpoint}`)
    let response = await request({
      url: endpoint,
      method: 'GET',
      headers: getForwardHeader(reqHeader),
      json: true
    })
    // logger.profile(`Profile service ${endpoint}`)
    return response
  } catch (err) {
    throw err
  }
}

app.get('/long', asyncHandler(async (req, res, next) => {
  // process.env.SERVICE1_HOST = 'http://localhost:8001/service1'
  let response = await callService(process.env.SERVICE1_HOST, req.headers)
  res.json(response)
}))

app.get('/service1', asyncHandler(async (req, res, next) => {
  // process.env.SERVICE2_HOST = 'http://localhost:8002/service2'
  let response = await callService(process.env.SERVICE2_HOST, req.headers)
  await delay(Math.ceil(Math.random() * 300)) // simulate loading
  res.json(response)
}))

app.get('/service2', asyncHandler(async (req, res, next) => {
  // process.env.SERVICE3_HOST = 'http://localhost:8080/service3'
  // process.env.SERVICE4_HOST = 'http://localhost:8080/service4'
  await callService(process.env.SERVICE3_HOST, req.headers)
  let response = await callService(process.env.SERVICE4_HOST, req.headers)
  res.json(response)
}))

app.get('/service3', asyncHandler(async (req, res, next) => {
  logger.info('service3 quick call')
  res.json({success: true, service: 3})
}))

app.get('/service5', asyncHandler(async (req, res, next) => {
  let response = await callService(process.env.SERVICEJ_HOST, req.headers)
  res.json({message: response})
}))

app.get('/service4', asyncHandler(async (req, res, next) => {
  // request.instrumentation.remoteServiceName = 'petApi'
  let endpoint = 'https://sample-api.ntdev.be/petstore/pets'
  logger.info(`Calling service ${endpoint}`)
  // logger.profile(`Calling service ${endpoint}`)
  let body = await request({
    url: endpoint,
    method: 'GET',
    json: true
  })
  // logger.profile(`Calling service ${endpoint}`)
  res.json(body)
}))

// liveness probe
app.get('/live', (req, res) => {
  res.json({status: 'ok'})
})

// default error hendler
app.use(function (err, req, res, next) {
  logger.error(err.message)
  res.status(err.status || 500).json({message: err.message})
})

const port = process.env.NODE_PORT || 8080
const server = app.listen(port, () => {
  // logger.info(process.pid)
  logger.info(`Example app listening on port ${port}!`)
  logger.debug(`SAMPLE_VAR_1=${process.env.SAMPLE_VAR_1}`)
  logger.debug(`SAMPLE_SECRET_1=${process.env.SAMPLE_SECRET_1}`)
})

// graceful shutdown
function onSignal () {
  logger.info('Cleanup resources')
  return Promise.all([
    new Promise(resolve => {
      // simulate shut down DB connection
      setTimeout(() => {
        logger.info('DB cleanup complete')
        resolve()
      }, 10000)
    })
  ])
}

function onShutdown () {
  logger.info('Shutdown completed')
}

function healthCheck () {
  return Promise.resolve({msg: 'ok'})
}

function beforeShutdown () {
  logger.info('Graceful shutdown start')
  status.shutdown = true
  return delay(6000) // endpoint /health return 500 error, set double time for readiness probe
}

const terminusOptions = {
  healthChecks: {
    '/health': healthCheck
  },
  timeout: 6000,
  beforeShutdown,
  onSignal,
  onShutdown,
  logger
}
createTerminus(server, terminusOptions)

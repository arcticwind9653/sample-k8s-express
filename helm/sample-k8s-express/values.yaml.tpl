# Default values for sample-k8s-express.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.
env:
  SAMPLE_VAR_1: $SAMPLE_VAR_1
  APP_VERSION: $IMAGE_TAG
  SERVICE1_HOST: $SERVICE1_HOST
  SERVICE2_HOST: $SERVICE2_HOST
  SERVICE3_HOST: $SERVICE3_HOST
  SERVICE4_HOST: $SERVICE4_HOST
  SERVICEJ_HOST: $SERVICEJ_HOST
  
secret:
  SAMPLE_SECRET_1: $SAMPLE_SECRET_1

# Enable the app log in ELK 
# MUST fill in custom.lable.scope and custom.lable.service at below
# that will be used as log index
log:
  elk:
    enabled: true

# Custom label for CI/CD and log use
custom:
  deployment:
    environment: $ENVIRONMENT
    hostname: $APP_HOSTNAME
    strategyType: $K8_DEPLOYMENT_STRATEGY # RollingUpdate / Recreate
  lable:
    scope: $SCOPE_NAME
    service: $SERVICE_NAME

auth:
  idp:
    endpoint: $IDP_ENDPOINT
    audiences:
      - $IDP_CLIENT_ID
  apiKey: $API_KEYS

# Enable istio proxy for this app
# if disable, you will miss tracing, traffic control feature provided by istio
# Suggest turn that on 
istio:
  sidecar:
    inject: true
  gateway:
    name: $K8_GW_NAME

replicaCount: 1

image:
  repository: 'replaced_by_CICD'
  tag: 'replaced_by_CICD'
  pullPolicy: IfNotPresent
  imagePullSecrets: $K8S_REPO_PULL_SECRET

nameOverride: $STACK_NAME
fullnameOverride: $APP_NAME

service:
  type: ClusterIP
  port: 8080

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: $APP_NAME.local
      paths: []

  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 512Mi
  # requests:
  #   cpu: 100m
  #   memory: 256Mi

nodeSelector: {}

tolerations: []

affinity: {}

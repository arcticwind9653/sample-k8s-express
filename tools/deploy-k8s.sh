#!/bin/bash  

# Before run this script, you need to login your docker and kubectl with correct access right
IMAGE_REPO=registry.cn-shenzhen.aliyuncs.com/newtype-dev/sample-express-api-test
IMAGE_TAG=1.0.2-local
HELM_CHART_NAME=sample-k8s-express-test

# npm install
# docker build ./ -t "$IMAGE_REPO:$IMAGE_TAG"
# docker push "$IMAGE_REPO:$IMAGE_TAG"
helm upgrade $HELM_CHART_NAME ./helm/sample-k8s-express/ -i -f ./helm/sample-k8s-express/values.test.yaml --debug --reuse-values --namespace test --set image.repository=$IMAGE_REPO --set image.tag=$IMAGE_TAG